set(LOGGER_INTEFACE logger_interface)

add_library(${LOGGER_INTEFACE} INTERFACE)
target_include_directories(
    ${LOGGER_INTEFACE}
    INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})
