#pragma once

#include "LoggerImpl.hpp" // include implementation of platform specific macro definition

/** ELOG_FAT - fatal errors log stream */
#ifndef ELOG_FAT
#error ELOG_FAT is not defined
#endif //  ELOG_FAT

/** ELOG_INF - informational logs stream */
#ifndef ELOG_INF
#error ELOG_INF is not defined
#endif //  ELOG_INF

namespace Logger {

/** @brief Initialize logger system. */
void init();

} // namespace Logger
