#include "Logger.hpp"

#include <iostream>

namespace Logger {

void init()
{
    std::cout << "Logger (IMPL2) initialization." << std::endl;
}

} // namespace Logger
