#include "Logger.hpp"

#include <iostream>

namespace Logger {

void init()
{
    std::cout << "Logger (IMPL1) initialization." << std::endl;
}

} // namespace Logger