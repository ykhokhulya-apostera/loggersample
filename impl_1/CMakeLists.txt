set(LOGGER_IMPL logger_impl)

add_library(${LOGGER_IMPL} STATIC
    LoggerImpl.cpp)
target_link_libraries(
    ${LOGGER_IMPL}
    PUBLIC logger_interface)
target_include_directories(
    ${LOGGER_IMPL}
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
