#include <cstdlib>

#include "Logger.hpp"

int main(int /*argc*/, const char** /*argv*/)
{
    Logger::init();

    ELOG_INF << "Info message." << std::endl;
    ELOG_FAT << "Fatal error." << std::endl;

    return EXIT_SUCCESS;
}